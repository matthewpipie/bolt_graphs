.PHONY: all clean

graphgen: graphgen.cpp
	g++ graphgen.cpp -o graphgen -std=c++11

clean:
	rm -rf graphgen
